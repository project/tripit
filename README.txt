Tripit
=============
This module provides a block with the Tripit Blog Badge
and a block per user with the user Tripit id configured.

see http://help.tripit.com/entries/89078-Blog-badge


Installation
-------------
Install this module as usual as per
http://drupal.org/documentation/install/modules-themes/modules-7


Configuration
--------------
Enable Blog Badge on the Tripit configuration page and copy the Tripit id.
https://www.tripit.com/account/edit/section/publishing_options

Add the Tripit id for the site under the Tripit settings page
admin/config/services/tripit

Each user with permission has the ability to add a Tripit block under
user/{uid}/tripit
